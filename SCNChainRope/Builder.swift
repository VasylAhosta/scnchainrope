//
// Created by Wasyl Ahosta on 2019-04-18.
// Copyright (c) 2019 Mordor Solutions. All rights reserved.
//

import Foundation
import SceneKit

final class Builder {

    func makeRing() -> SCNNode {
        let geometry = SCNTorus(ringRadius: 0.5, pipeRadius: 0.1)
        geometry.materials.first?.diffuse.contents = UIColor.black
        let node = SCNNode(geometry: geometry)
        node.physicsBody = SCNPhysicsBody(type: .dynamic, shape: nil)
        node.physicsBody?.mass = 50.0
        node.physicsBody?.angularVelocityFactor = SCNVector3(1, 0, 1)
        node.physicsBody?.angularRestingThreshold = 0.6
        node.physicsBody?.linearRestingThreshold = 0.6
        return node
    }

    func makeHolder() -> SCNNode {
        let node = SCNNode()
        return node
    }

    func makeRope(_ scene: SCNScene) -> [SCNNode] {
        var links = [makeLink()]
        for i in 0..<3 {
            let link = makeLink(index: i + 1)
            join(link.physicsBody!, with:links.last!.physicsBody!, in:scene.physicsWorld)
            links.append(link)
            link.position = SCNVector3(0, -i + 1, 0)
        }
        return links
    }

    func makeLink(radius: CGFloat = 0.1, index: Int = 0) -> SCNNode {
        let geometry = SCNSphere(radius: radius)//0.1)
//        let geometry = SCNBox(width: 0.4, height: 0.4, length: 1, chamferRadius: 0)
        geometry.materials.first?.diffuse.contents = UIColor.yellow
        let node = SCNNode(geometry: geometry)
        node.physicsBody = SCNPhysicsBody(type: .dynamic, shape: nil)
        node.physicsBody?.mass = 100//CGFloat(10 - index) + 1
//        node.physicsBody?.restitution = 0
//        node.physicsBody?.friction = 0
        node.physicsBody?.angularVelocityFactor = SCNVector3(1, 0, 1)
//        node.physicsBody?.angularRestingThreshold = 0.6
//        node.physicsBody?.linearRestingThreshold = 0.6
        return node
    }

    private func join(_ bodyA: SCNPhysicsBody, with bodyB: SCNPhysicsBody, in world: SCNPhysicsWorld) {
//        let joint = SCNPhysicsBallSocketJoint(bodyA: bodyA, anchorA: SCNVector3(0, -0.55, 0),
//                                              bodyB: bodyB, anchorB: SCNVector3(0, 0.55, 0))

        let joint = SCNPhysicsSliderJoint(bodyA: bodyA, axisA: SCNVector3(0, 0, 1), anchorA: SCNVector3(0, -0.55, 0),
                                          bodyB: bodyB, axisB: SCNVector3(0, 0, 1), anchorB: SCNVector3(0, 0.55, 0))
        joint.minimumLinearLimit = 0
        joint.maximumLinearLimit = 0
        joint.minimumAngularLimit = .angle(-135)
        joint.maximumAngularLimit = .angle(200)

        world.addBehavior(joint)
    }

    func makeCeiling() -> SCNNode {
        let geometry = SCNBox(width: 0.2, height: 0.1, length: 1, chamferRadius: 0.0)
        geometry.materials.forEach { $0.diffuse.contents = UIColor.red }
        let node = SCNNode(geometry: geometry)
        node.position = SCNVector3(x: 0, y: 5, z: 0)
        node.physicsBody = SCNPhysicsBody(type: .static, shape: nil)
        return node
    }

    func makeAll(_ scene: SCNScene) -> SCNNode {
        let ceiling = makeCeiling()
        let rope = makeRope(scene)
        attach(rope, to: ceiling, in: scene.physicsWorld)
        //rope.last?.physicsBody?.mass = 10
        //let ring = makeRing()
        //ring.position = rope.last?.position ?? SCNVector3(0, 0, 0)
        //attach(ring, to: rope, in: scene.physicsWorld)
        scene.rootNode.addChildNode(ceiling)
        rope.forEach { scene.rootNode.addChildNode($0) }
        //scene.rootNode.addChildNode(ring)
        return ceiling
    }

    private func attach(_ rope : [SCNNode], to ceiling: SCNNode, in world: SCNPhysicsWorld) {
        let joint = SCNPhysicsSliderJoint(bodyA: ceiling.physicsBody!, axisA: SCNVector3(0, 0, 1), anchorA: SCNVector3(0, -0.1, 0),
                                          bodyB: rope.first!.physicsBody!, axisB: SCNVector3(0, 0, 1), anchorB: SCNVector3(0, -0.55, 0))
        joint.minimumLinearLimit = 0
        joint.maximumLinearLimit = 0
        joint.minimumAngularLimit = .angle(-135)
        joint.maximumAngularLimit = .angle(220)
//        let joint = SCNPhysicsBallSocketJoint(bodyA: rope.first!.physicsBody!, anchorA: SCNVector3(0, -0.1, 0),
//                                              bodyB: ceiling.physicsBody!, anchorB: SCNVector3(0, -0.55, 0))
        world.addBehavior(joint)
    }

    private func attach(_ ring: SCNNode, to rope: [SCNNode], in world: SCNPhysicsWorld) {
        let joint = SCNPhysicsBallSocketJoint(bodyA: rope.last!.physicsBody!, anchorA: SCNVector3(x: 0, y: 0.55, z: 0),
                                              bodyB: ring.physicsBody!, anchorB: SCNVector3(x: 0 , y: -0.55, z: 0))
        world.addBehavior(joint)
    }

}

extension CGFloat {

    static func angle(_ a: CGFloat) -> CGFloat {
        return a * CGFloat.pi / 180.0
    }

}