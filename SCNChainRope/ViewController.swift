//
//  ViewController.swift
//  SCNChainRope
//
//  Created by Wasyl Ahosta on 4/18/19.
//  Copyright © 2019 Mordor Solutions. All rights reserved.
//

import UIKit
import SceneKit

final class ViewController: UIViewController {

    @IBOutlet private var sceneView: SCNView! {
        didSet {
            sceneView.antialiasingMode = .multisampling4X
            sceneView.scene = scene
        }
    }

    private lazy var scene: SCNScene = self.makeScene()

    private func makeScene() -> SCNScene {
        let scene = SCNScene()
        scene.background.contents = UIColor.clear
        scene.rootNode.addChildNode(cameraNode)
        return scene
    }

    private lazy var cameraNode: SCNNode = self.makeCameraNode()

    private func makeCameraNode() -> SCNNode {
        let cameraNode = SCNNode()
        let camera = SCNCamera()
        camera.zFar = 1000
        cameraNode.camera = camera
        return cameraNode
    }

    private var movableNode: SCNNode?
    private let builder = Builder()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupChain()
//        setupCylinder()
    }

    private func addPhysicBody(to node: SCNNode) {
        node.childNodes.forEach { addPhysicBody(to: $0) }
        guard let name = node.name, name.hasPrefix("joint") else { return }
        let shape = SCNPhysicsShape(geometry: Builder().makeLink(radius: 1).geometry!)
        node.physicsBody = SCNPhysicsBody(type: .dynamic, shape: shape)
        node.physicsBody?.mass = 20
//        node.physicsBody?.momentOfInertia = SCNVector3(x: 5, y: 5, z: 5)
//        node.physicsBody?.usesDefaultMomentOfInertia = false
        print("Added physics to \(name), \(node.position)")
    }

    @IBAction func panGestureAction(_ sender: UIPanGestureRecognizer) {
        movableNode?.position.x += Float(sender.translation(in: view).x) / 100.0
        movableNode?.position.z += Float(sender.translation(in: view).y) / 100.0
        sender.setTranslation(.zero, in: view)
    }
    
}

private extension ViewController {

    func setupCylinder() {
        let url = Bundle.main.url(forResource: "cylinder_rigged_test", withExtension: "dae")
        let source = SCNSceneSource(url: url!)!
        let node = source.entryWithIdentifier("node/5", withClass: SCNNode.self)!
        scene.rootNode.addChildNode(node)
        cameraNode.position = SCNVector3(x: 0, y: 0, z: 40)
        addPhysicBody(to: node)
        let ceiling = builder.makeCeiling()
        ceiling.position = SCNVector3(0, 20, 0)
        scene.rootNode.addChildNode(ceiling)
        movableNode = ceiling
        let bone1 = node.childNode(withName: "joint1", recursively: true)!
        bone1.physicsBody?.angularVelocityFactor = SCNVector3(x: 1.0, y: 0.0, z: 1.0)
        let joint = SCNPhysicsBallSocketJoint(bodyA: ceiling.physicsBody!, anchorA: SCNVector3(0, -0.1, 0), bodyB: bone1.physicsBody!, anchorB: SCNVector3(-1, 0, 0))
        scene.physicsWorld.addBehavior(joint)
        let bone2 = node.childNode(withName: "joint2", recursively: true)!
        recursivelyJoin(bone1, bone2, offset: 8)
        addSphere()
    }

    private func recursivelyJoin(_ nodeA: SCNNode, _ nodeB: SCNNode, offset: CGFloat = 0) {
        if offset > 0 {
            scene.physicsWorld.addBehavior(SCNPhysicsBallSocketJoint(bodyA: nodeA.physicsBody!, anchorA: SCNVector3(0, offset, 0), bodyB: nodeB.physicsBody!, anchorB: SCNVector3(0, 0, 0)))
        } else {
            scene.physicsWorld.addBehavior(SCNPhysicsBallSocketJoint(bodyA: nodeA.physicsBody!, anchorA: SCNVector3(2, offset, 0), bodyB: nodeB.physicsBody!, anchorB: SCNVector3(-2, 0, 0)))
        }
        if let nodeC = nodeB.childNodes.first {
            recursivelyJoin(nodeB, nodeC)
        }
        nodeB.physicsBody?.angularVelocityFactor = SCNVector3(x: 1.0, y: 0.0, z: 1.0)
    }

    private func addSphere() {
        let node = builder.makeLink(radius: 5)
        node.position = SCNVector3(10, 7, 0)
        node.physicsBody?.type = .kinematic
        scene.rootNode.addChildNode(node)
    }

}

private extension ViewController {

    func setupChain() {
        movableNode = builder.makeAll(scene)
        cameraNode.position = SCNVector3(x: 0, y: 0.5, z: 15)
    }

}
